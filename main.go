package main

import "fmt"

func main() {
	fmt.Println(makeGreeting())
}


const greeting = "Hello GitLab!!!"

func makeGreeting() string {
	return greeting
}
